﻿using ExifLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afficheur
{
    class DataModel
    {
        public DataModel() { }
        public List<JPGFileInfo> Fichiers { get; set; }
        public DataModel(String Chemin)
        {
            Fichiers = new List<JPGFileInfo>();
            string[] fichiers = Directory.GetFiles(Chemin);
            if (fichiers != null)
            {
                fichiers.ToList().Where(c => c.ToLower().EndsWith("jpg")).ToList().ForEach(c => Fichiers.Add(new JPGFileInfo(c)));
            }
        }
        public List<DirectoryItem> GetItems(string path)
        {
            var items = new List<DirectoryItem>(); // je l'ai modifié en ajoutant DirectoryItem (a revoir)
            var dirInfo = new DirectoryInfo(path);
            foreach (var directory in dirInfo.GetDirectories())
            {
                var item = new DirectoryItem
                {
                    Name = directory.Name,
                    Path = directory.FullName,
                    Items = GetItems(directory.FullName)
                };
                items.Add(item);
            }
            return items;
        }

        public class Photo
        {
            public Photo()
            {
                this.MotClés = new HashSet<MotClé>();
            }
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime Date { get; set; }
            public int Année { get; set; }
            public int Mois { get; set; }
            public int Jour { get; set; }
            public virtual ICollection<MotClé> MotClés { get; set; }
        }
        public class MotClé
        {
            public MotClé()
            {
                this.Photos = new HashSet<Photo>();
            }
            public int Id { get; set; }
            public string Name { get; set; }
            public virtual ICollection<Photo> Photos { get; set; }
        }

    }

}
