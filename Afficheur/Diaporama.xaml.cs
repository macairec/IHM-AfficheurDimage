﻿using ExifLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Afficheur
{
    /// <summary>
    /// Logique d'interaction pour Diaporama.xaml
    /// </summary>
    public partial class Diaporama : Window
    {
        int nbImage;
        int current = 0;
        public DispatcherTimer timer{ get; set;}
        public Diaporama(object Context)
        {
            InitializeComponent();
            DataContext = Context;
            image.DataContext = ((DataModel)DataContext).Fichiers[current];
            nbImage = ((DataModel)DataContext).Fichiers.Count;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 2);
            timer.Tick += timer_Tick;
            timer.Start();
            this.PreviewKeyDown += new KeyEventHandler(Controls);

        }

        private void Controls(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
            if (e.Key == Key.Right)
            {
                current = (++current) % nbImage;
            }
            if(e.Key == Key.Left)
            {
                current--;
                if (current < 0)
                {
                    current = nbImage - 1;
                
                }
                
            }
            if(e.Key == Key.Space)
            {
                if (timer != null)
                {
                    timer.Stop();
                }
                
            }

            image.DataContext = ((DataModel)DataContext).Fichiers[current];
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (current < nbImage)
            {
                image.DataContext = ((DataModel)DataContext).Fichiers[current++];
            }
            else
            {
                current = 0;
            }

        }


    }
    

}
